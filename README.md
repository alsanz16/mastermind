# Mastermind
Implementation of the Knuth algorithm for mastermind, as explained here: https://www.cs.uni.edu/~wallingf/teaching/cs3530/resources/knuth-mastermind.pdf

### Usage
Executing the program is straightforward. All the logic is in the module `mastermind.py`.
The module `mastermind_view.py` includes helpers to try and visualize the algorithm, so I recommend you use this package to try the algorithm

     >>> from mastermind_view import guess_code
     >>> guess_code()
    ============================================================
            SECRET CODE: 🟡 🟡 🟣 🟢
    ============================================================
        Guess		Hint
        🔴 🔴 🔵 🔵
        🟡 🟣 🟣 🟢   	⚫ ⚫ ⚫
        🟠 🟣 🟡 🟡   	⚪ ⚪ ⚪
        🟡 🟠 🟣 🟢   	⚫ ⚫ ⚫
        🟡 🟡 🟣 🟢   	⚫ ⚫ ⚫ ⚫

You can play with a user selected code:

    >>> guess_code(code=(1,2,3,4))
    ============================================================
            SECRET CODE: 🔵 🟡 🟢 🟠
    ============================================================
        Guess		Hint
        🔴 🔴 🔵 🔵   	⚪
        🟠 🟠 🟡 🔴   	⚪ ⚪
        🟡 🟣 🔴 🟡   	⚪
        🔵 🟡 🟠 🟢   	⚫ ⚫ ⚪ ⚪
        🔵 🟡 🟢 🟠   	⚫ ⚫ ⚫ ⚫

