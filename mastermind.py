'''
Implements methods to visualize Mastermind algorithm
>> guess_code(): solves the mastermind game for a random code
>> guess_code(code = (1,2,34)): solves the mastermind game for a specified code
'''
from collections import Counter, defaultdict
from itertools import product
from functools import cache, partial
import random

N = 6
KEYS = 4

ALL_CODES = set(product(range(N), repeat=KEYS))

@cache
def check(code, guess):
    """
    Evaluates hint created by following mastermind rules when attempting
    to guess code by using guess

    Args:
        code: the secret code
        guess: code proposed
    Returns:
        (number of black stones, number of white stones)
    """
    present = sum((Counter(code) & Counter(guess)).values())
    correct = sum(c == g for c, g in zip(code, guess))
    return correct, present - correct

def solve(check_function, default_guess = (0,0,1,1)):
    """
    Solves mastermind using Knuth algorithm.  
    Original algorithm comes from: https://www.cs.uni.edu/~wallingf/teaching/cs3530/resources/knuth-mastermind.pdf

    Args:
        check_function: a function that takes a guess and returns a hint
        default_guess: initial guess of the algorithm
    Yields:
        (guess, hint) until the code is guessed
    """
    guess, hint = default_guess, check_function(default_guess)
    solutions = ALL_CODES
    yield guess, hint

    while hint != (KEYS, 0):
        solutions = {code for code in solutions if check(code, guess) == hint}
        information = lambda guess: max(Counter(check(sol, guess) for sol in solutions).values())
        # Reward guesses in solutions so the candidate is chosen from solutions if possible
        guess = min(ALL_CODES, key = lambda guess: information(guess) - 0.5*(guess in solutions))
        
        hint = check_function(guess)
        yield guess, hint

def solve_with_code(code):
    return solve(partial(check, code))

def random_code():
    return random.choice(tuple(product(range(N), repeat=KEYS)))
