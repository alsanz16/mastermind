from mastermind import *
from tabulate import tabulate
import random

CODE_COLORS = ('🔴', '🔵', '🟡', '🟢', '🟠', '🟣')
HINT_COLORS = ('⚫', '⚪')

SEPARATOR = "=" * 60 
ARROW = "---> ";

def code_to_string(code, spacing = True):
    spacing = ' ' if spacing else ''
    return spacing.join(CODE_COLORS[index] for index in code)

def hint_to_string(hint, spacing = True):
    spacing = ' ' if spacing else ''
    success, present = hint
    SUCCESS_COLOR, PRESENT_COLOR = HINT_COLORS
    return spacing.join(success*SUCCESS_COLOR + present * PRESENT_COLOR)

def guess_code(code = None):
    if not code:
        code = random_code()
    print(f"{SEPARATOR}\n\t\tSECRET CODE: {code_to_string(code)}\n{SEPARATOR}")

    print("\tGuess\t\tHint") 
    for (guess, hint) in solve_with_code(code):
        print(f"\t{code_to_string(guess):10}\t{hint_to_string(hint):10}")
